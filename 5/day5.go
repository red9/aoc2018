package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

type Result struct {
	Char   byte
	Length int
}

const caseOffset = 'a' - 'A'

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input := string(f)
	fmt.Printf("The output for file %s has length %d.\n", os.Args[1], len(bruteforce(input, 0)))
	ch := make(chan Result)
	for _, c := range "abcdefghijklmnopqrstuvwxyz" {
		go func(input string, ignore byte, ch chan Result) {
			output := bruteforce(input, ignore)
			ch <- Result{ignore, len(output)}
		}(input, byte(c), ch)
	}
	min := Result{0, len(input)}
	for i := 0; i < 26; i++ {
		r := <-ch
		if r.Length < min.Length {
			min = r
		}
	}
	fmt.Printf("The output for file %s with character %c removed has length %d.\n", os.Args[1], min.Char, min.Length)
}

//ignore should be the byte value of the character you want to ignore, in lowercase
//if you want to ignore nothing, set ignore = 0
func bruteforce(input string, ignore byte) string {
	for {
		new_string := ""
		pivot := 0
		for i := 0; i < len(input)-1; i++ {
			if ignore != 0 && (input[i] == ignore || input[i] == ignore-caseOffset) {
				new_string += input[pivot:i]
				pivot = i + 1
				continue
			}
			if input[i] == input[i+1]+caseOffset || input[i] == input[i+1]-caseOffset {
				new_string += input[pivot:i]
				pivot = i + 2
				//skip over the next character
				i++
			}
		}
		//finally, add the remaining part of the input string
		if pivot < len(input) {
			new_string += input[pivot:]
		}
		//if the input did not change in this iteration of the function, we have found all pairs, and we are done
		if len(input) == len(new_string) {
			return new_string
		}
		//otherwise, set input to new_string and start again
		input = new_string
	}
}
