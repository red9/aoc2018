package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Date struct {
	Month int
	Day   int
}

type Record struct {
	Date        Date
	TimesAsleep []int
	TimesAwake  []int
}

func (r *Record) append(awake bool, m int) {
	if awake {
		r.TimesAsleep = append(r.TimesAsleep, m)
	} else {
		r.TimesAwake = append(r.TimesAwake, m)
	}
}

func (r *Record) count() (asleep int) {
	awakes := r.TimesAwake
	asleeps := r.TimesAsleep
	for {
		//if we have parsed everything, the current count of minutes asleep is accurate.
		if len(awakes) == 0 && len(asleeps) == 0 {
			return
			//Else, if the "awakes" slice is empty, add the next asleep time to the running count and return.
		} else if len(awakes) == 0 {
			return asleep + (60 - asleeps[0])
		}
		//Else, parse next pair of values and remove them from the slices.
		asleep += awakes[0] - asleeps[0]
		awakes = awakes[1:]
		asleeps = asleeps[1:]
	}
}

func (r *Record) enumerate() (minutes [60]bool) {
	awakes := r.TimesAwake
	asleeps := r.TimesAsleep
	for {
		//if we have parsed everything, the current array is accurate.
		if len(awakes) == 0 && len(asleeps) == 0 {
			return
			//Else, if the "awakes" slice is empty set all remaining indices from the next fall-asleep time to true
		} else if len(awakes) == 0 {
			a := minutes[asleeps[0]:]
			for i := range a {
				a[i] = true
			}
		}
		//Else, parse next pair of values and remove them from the slices.
		a := minutes[asleeps[0]:awakes[0]]
		for i := range a {
			a[i] = true
		}
		awakes = awakes[1:]
		asleeps = asleeps[1:]
	}
}

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	m := make(map[int][]*Record)
	for i := 0; i < len(input_s); {
		idRegex := regexp.MustCompile("#[0-9]+")
		dateRegex := regexp.MustCompile("[0-9]{2}-[0-9]{2} [0-9]{2}")
		minutesRegex := regexp.MustCompile(":[0-9]{2}")
		s := string(idRegex.Find([]byte(input_s[i])))
		date := string(dateRegex.Find([]byte(input_s[i])))
		id, _ := strconv.Atoi(s[1:])
		month, _ := strconv.Atoi(date[:2])
		day, _ := strconv.Atoi(date[3:5])
		if date[6:] == "23" {
			day++
		}
		i++
		awake := true
		record := &Record{
			Date{month, day},
			[]int{},
			[]int{},
		}
		// create a new record if we do not have one yet
		e, ok := m[id]
		if !ok {
			m[id] = []*Record{record}
		} else {
			m[id] = append(e, record)
		}
		for {
			// if the length is over 32, then nothing happened this shift and there is a new record to be parsed
			if i >= len(input_s) || len(input_s[i]) > 32 {
				break
			}
			mins := string(minutesRegex.Find([]byte(input_s[i])))
			min, _ := strconv.Atoi(mins[1:])
			record.append(awake, min)
			awake = !awake
			i++
		}
	}
	a := a(m)
	fmt.Println(
		"For the input text", os.Args[1], "the guard with the most minutes asleep was", a[0],
		"and the minute they were asleep on the most was", a[1], ". This gives a checksum of", a[0]*a[1])
	b := b(m)
	fmt.Println(
		"For the input text", os.Args[1], "the guard who is most frequently asleep on the same minute is guard",
		b[0], "on the minute", b[1], ". This gives a checksum of", b[0]*b[1])
}

func a(input map[int][]*Record) [2]int {
	var id, max int
	//fetch the ID of the guard who's asleep the most
	for k, v := range input {
		var count int
		for _, r := range v {
			count += r.count()
		}
		if count > max {
			id = k
			max = count
		}
	}
	records := input[id]
	var mins [60]int
	//count time asleep for each minute
	for _, v := range records {
		for i, asleep := range v.enumerate() {
			if asleep {
				mins[i] += 1
			}
		}
	}
	//count the minute with the most time asleep
	max = 0
	for i, v := range mins {
		if v > mins[max] {
			max = i
		}
	}
	//return the id of the most sleepy guard, and the minute they're asleep on the most
	return [2]int{id, max}
}

func b(input map[int][]*Record) [2]int {
	var mins [60]map[int]int
	for i := range mins {
		mins[i] = make(map[int]int)
	}
	var max_minute, max_id int
	for id, v := range input {
		for _, r := range v {
			for minute, asleep := range r.enumerate() {
				if asleep {
					mins[minute][id] += 1
					if mins[minute][id] > mins[max_minute][max_id] {
						max_minute = minute
						max_id = id
					}
				}
			}
		}
	}
	return [2]int{max_id, max_minute}
}
