package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	fS := string(f)
	fA := strings.Split(fS, "\n")
	var input []int64
	for _, v := range fA {
		i, err := strconv.ParseInt(v, 10, 64)
		if err != nil {
			fmt.Print(err)
		}
		input = append(input, i)
	}
	m := make(map[int64]int)
	var repeats bool
	var sum int64
	for !(repeats) {
		m[0] = 1
		for _, v := range input {
			sum += v
			_, ok := m[sum]
			if ok {
				fmt.Print("The value ", sum, " was reached twice.\n")
				repeats = true
			}
			m[sum] = 1
		}
	}
}
