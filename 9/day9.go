package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	var players, marble int
	fmt.Sscanf(string(f), "%d players; last marble is worth %d points", &players, &marble)
	fmt.Printf("With the input %s, the winning score was %d\n", os.Args[1], a(players, marble))
	fmt.Printf("With the input %s, the winning score with a hundred times the number of marbles was %d\n",
		os.Args[1], b(players, marble*100))
}

func a(players, marbles int) (score int) {
	//index tracks the current marble
	//next tracks the value of the next marble
	//the gamestate is initialized to the board {0, 1}.
	index := 1
	gamestate := []int{0, 1}
	scores := make(map[int]int)
	for next := 2; next <= marbles; next++ {
		if next%23 == 0 {
			//if the next marble is a multiple of 23, remove the marble seven indices prior to the current marble
			//and add it and the next marble to the current player's score
			scores[next%players] += next
			removeIndex := (len(gamestate) + index - 7) % len(gamestate)
			scores[next%players] += gamestate[removeIndex]
			gamestate = append(gamestate[:removeIndex], gamestate[removeIndex+1:]...)
			//then set the current index to whichever element is now at removeIndex, or the first element
			index = removeIndex % len(gamestate)
		} else {
			nextIndex := (index + 2) % len(gamestate)
			index = nextIndex
			gamestate = append(gamestate, 0)
			copy(gamestate[nextIndex+1:], gamestate[nextIndex:])
			gamestate[nextIndex] = next
		}
	}
	for _, v := range scores {
		if v > score {
			score = v
		}
	}
	return
}

type Node struct {
	value      int
	last, next *Node
}

func b(players, marbles int) (score int) {
	current := &Node{0, nil, nil}
	one := &Node{1, current, current}
	current.last = one
	current.next = one
	scores := make(map[int]int)
	for next := 2; next <= marbles; next++ {
		if next%23 == 0 {
			//move counterclockwise six indices
			current = current.last.last.last.last.last.last //this is an abomination
			//increment the current player's score by the requisite amount
			scores[next%players] += current.last.value + next
			//remove the previous element
			current.last = current.last.last
			current.last.next = current
		} else {
			//move clockwise one index
			current = current.next
			//insert the next marble
			n := &Node{next, current, current.next}
			current.next = n
			n.next.last = n
			current = n
		}
	}
	for _, v := range scores {
		if v > score {
			score = v
		}
	}
	return
}
