package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Point struct {
	x, y int
}

func (p *Point) distanceTo(q *Point) int {
	x := p.x - q.x
	if x < 0 {
		x = -x
	}
	y := p.y - q.y
	if y < 0 {
		y = -y
	}
	return x + y
}

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	var input []Point
	var max int
	for _, s := range input_s {
		var x, y int
		fmt.Sscanf(s, "%d, %d", &x, &y)
		input = append(input, Point{x, y})
		if x > max {
			max = x
		}
		if y > max {
			max = y
		}
	}
	p, a := a_force(input, max+2)
	fmt.Printf("The largest area for %s is point %v with area %d\n", os.Args[1], p, a)
	fmt.Printf("The area containing all locations with total distance <10000 is %d\n", b_bruteforce(input, max))
}

func a_force(input []Point, bound int) (Point, int) {
	m := make(map[Point]int)
	bad_points := make(map[Point]bool)
	for _, p := range input {
		m[p] = 0
	}

	for x := 0; x < bound; x++ {
		for y := 0; y < bound; y++ {
			q := Point{x, y}
			min := 2 * bound
			var min_point Point
			var many_min bool
			for _, p := range input {
				d := p.distanceTo(&q)
				if d < min {
					min = d
					min_point = p
					many_min = false
				} else if d == min {
					many_min = true
				}
			}
			if x == 0 || y == 0 || x == bound-1 || y == bound-1 {
				bad_points[min_point] = true
			}
			if !many_min {
				m[min_point] += 1
			}
		}
	}
	var max_point Point
	var max_area int
	for k, v := range m {
		_, ok := bad_points[k]
		if !ok && v > max_area {
			max_point = k
			max_area = v
		}
	}
	return max_point, max_area
}

func b_bruteforce(input []Point, bound int) (area int) {
	for x := -200; x < bound+200; x++ {
		for y := -200; y < bound+200; y++ {
			q := &Point{x, y}
			var sum int
			for _, p := range input {
				sum += p.distanceTo(q)
			}
			if sum >= 10000 {
				continue
			}
			area++
		}
	}
	return
}
