package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input := strings.Split(string(f), "\n")
	fmt.Println("The checksum for "+os.Args[1]+" is:", a(input))
	fmt.Println("The shared characters for", os.Args[1], "are:", b(input))
}

func a(input []string) (sum int) {
	var twos, threes int
	for _, v := range input {
		var two, three bool
		for _, c := range "abcdefghijklmnopqrtsuvwxyz" {
			count := strings.Count(v, string(rune(c)))
			if count == 2 {
				two = true
			} else if count == 3 {
				three = true
			}
		}
		if two {
			twos += 1
		}
		if three {
			threes += 1
		}
	}
	sum = 1 * twos * threes
	return
}

func b(input []string) string {
	length := len(input[0])
	for i := 0; i < length; i++ {
		m := make(map[string]bool)
		for _, v := range input {
			ss := v[0:i] + v[i+1:length]
			_, ok := m[ss]
			if ok {
				return ss
			}
			m[ss] = true
		}
	}
	return ""
}
