package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type SparseMatrix struct {
	Elements map[Point]bool
}

type Point struct {
	x, y int
}

type Box struct {
	p      Point
	length int
	height int
}

func (A *SparseMatrix) AddBox(b Box) {
	for i := 0; i < b.length; i++ {
		for j := 0; j < b.height; j++ {
			q := Point{b.p.x + i, b.p.y + j}
			e, ok := A.Elements[q]
			if !ok {
				A.Elements[q] = false
			} else if !e {
				A.Elements[q] = true
			}
		}
	}
}

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	var input []Box
	for _, v := range input_s {
		var i, x, y, l, h int
		fmt.Sscanf(v, "#%d @ %d,%d: %dx%d", &i, &x, &y, &l, &h)
		input = append(input, Box{Point{x, y}, l, h})
	}
	fmt.Println("The number of overlapping claims for "+os.Args[1]+" is:", a(input))
}

func a(input []Box) int {
	m := SparseMatrix{map[Point]bool{}}
	for _, v := range input {
		m.AddBox(v)
	}
	var sum int
	for _, v := range m.Elements {
		if v {
			sum += 1
		}
	}
	return sum
}
