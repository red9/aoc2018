package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type SparseMatrix struct {
	Elements map[Point]int
	Claims   map[int]bool
}

type Point struct {
	x, y int
}

type Claim struct {
	p      Point
	length int
	height int
	uid    int
}

func (A *SparseMatrix) AddClaim(c Claim) {
	A.Claims[c.uid] = true
	for i := 0; i < c.length; i++ {
		for j := 0; j < c.height; j++ {
			q := Point{c.p.x + i, c.p.y + j}
			e, ok := A.Elements[q]
			if !ok {
				A.Elements[q] = c.uid
			} else {
				delete(A.Claims, c.uid)
				delete(A.Claims, e)
			}
		}
	}
}

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	var input []Claim
	for _, v := range input_s {
		var i, x, y, l, h int
		fmt.Sscanf(v, "#%d @ %d,%d: %dx%d", &i, &x, &y, &l, &h)
		input = append(input, Claim{Point{x, y}, l, h, i})
	}
	fmt.Println("The only non-overlapping claim for "+os.Args[1]+" is:", b(input))
}

func b(input []Claim) int {
	m := SparseMatrix{map[Point]int{}, map[int]bool{}}
	for _, v := range input {
		m.AddClaim(v)
	}
	for k, _ := range m.Claims {
		return k
	}
	return 0
}
