package main

import (
	"errors"
	"fmt"
	"github.com/fogleman/gg"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

type Star struct {
	x, y, vx, vy int
}

type Point struct {
	x, y int
}

func main() {
	rand.Seed(time.Now().Unix())
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	var input []Star
	for _, s := range input_s {
		star := Star{0, 0, 0, 0}
		fmt.Sscanf(s, "position=<%d, %d> velocity=<%d, %d>", &star.x, &star.y, &star.vx, &star.vy)
		input = append(input, star)
	}
	err := a(input)
	if err != nil {
		fmt.Println("Failed with error:", err)
	}
}

func a(input []Star) (err error) {
	for i := 0; i < 8192; i++ {
		height, width, _, _ := calculateBoundingBox(input, i*8)
		area := height * width
		if area < 10000 {
			for j := i * 8; j <= i*8+8; j++ {
				fmt.Printf("Saved file to %s\n", renderImage(input, j))
			}
			return nil
		}
	}
	return errors.New("bounding box does not become small enough")
}

func calculateBoundingBox(input []Star, λ int) (height, width, bottom, left int) {
	top := input[0].y + input[0].vy*λ
	bottom = input[0].y + input[0].vy*λ
	left = input[0].x + input[0].vx*λ
	right := input[0].x + input[0].vx*λ
	for _, s := range input {
		x := s.x + s.vx*λ
		y := s.y + s.vy*λ
		if y > top {
			top = y
		}
		if y < bottom {
			bottom = y
		}
		if x > right {
			right = x
		}
		if x < left {
			left = x
		}
	}
	return top - bottom, right - left, bottom, left
}

func renderImage(input []Star, λ int) (filename string) {
	const S = 1024
	dc := gg.NewContext(S, S)
	dc.SetRGB(1, 0, 0)
	dc.Scale(512, 512)
	height, width, bottom, left := calculateBoundingBox(input, λ)
	var size float64
	if height > width {
		size = float64(height)
	} else {
		size = float64(width)
	}
	for _, s := range input {
		x := float64(s.x+λ*s.vx-left) / size
		y := float64(s.y+λ*s.vy-bottom) / size
		dc.DrawPoint(x, y, 3)
		dc.Fill()
	}
	filename = fmt.Sprintf("%d_iterations.png", λ)
	dc.SavePNG(filename)
	return filename
}
