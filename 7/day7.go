package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

type Task struct {
	parents, children []*Task
	label             byte
}

type signal struct{}

var yes signal

func main() {
	f, _ := ioutil.ReadFile(os.Args[1])
	input_s := strings.Split(string(f), "\n")
	m := make(map[byte]*Task)
	for _, c := range "ABCDEFGHIJKLMNOPQRSTUVWXYZ" {
		m[byte(c)] = &Task{[]*Task{}, []*Task{}, byte(c)}
	}
	for _, s := range input_s {
		var parent, child byte
		fmt.Sscanf(s, "Step %c must be finished before step %c can begin.", &parent, &child)
		m[parent].children = append(m[parent].children, m[child])
		m[child].parents = append(m[child].parents, m[parent])
	}
	fmt.Printf("The order of tasks for %s is %s.\n", os.Args[1], a(m))
}

func a(input map[byte]*Task) (output string) {
	counted := make(map[*Task]signal)
	roots := make(map[*Task]signal)
	for _, v := range input {
		if len(v.parents) == 0 {
			roots[v] = yes
		}
	}
	for len(roots) != 0 {
		//find the root with the smallest label
		var min *Task
		for k, _ := range roots {
			if min == nil {
				min = k
			} else if k.label < min.label {
				min = k
			}
		}
		//log the smallest root
		output += string(min.label)
		counted[min] = yes
		//remove the smallest root and add its children if their parents have now all been counted
		delete(roots, min)
		for _, c := range min.children {
			var has_parents bool
			for _, p := range c.parents {
				_, ok := counted[p]
				if !ok {
					has_parents = true
				}
			}
			if !has_parents {
				roots[c] = yes
			}
		}
	}
	return
}
